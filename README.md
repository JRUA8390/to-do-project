# To-Do App

  - Browser Application that keeps track of your TO-DO lists

# Features
- Insert new To-Dos
- Delete To-Dos
- Search To-Dos
- Saves To-Dos after closing app

# Getting Started

##### 1. Clone the repo onto your local PC
##### 2. Install dependencies  
```bash
npm install
```
##### 3. Run the App
```bash
npm start
```
