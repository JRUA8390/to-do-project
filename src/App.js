import React, { Component } from 'react';
import './App.css';
import MenuBar from './Components/MenuBar';
import ToDoTable from './Components/Table';

class App extends Component {
  render() {
    return (
      <div>
      <MenuBar></MenuBar>
      <ToDoTable></ToDoTable>
      </div>
    );
  }
}

export default App;
