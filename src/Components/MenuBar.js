import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

const MenuBar = () => {
  return  <AppBar
      position="static"
      title="Title"
      >
      <Toolbar>
     <Typography variant="title" color="inherit">
       To-Do App
     </Typography>
   </Toolbar>
   </AppBar>
}

export default MenuBar;
