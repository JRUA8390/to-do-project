import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';


  const styles = {
    gridBorder: {
      backgroundColor: '#d6d7da',
      borderRadius: 8
    },
    listItemStyle: {
      border: '1px solid #164796',
      paddingTop:0,
      paddingBottom:0,
      marginLeft:25,
      background: 'white',
      height: 35,
      lineHeight: 60,
      width:218
    },
    inputField: {
    borderRadius: 4,
    backgroundColor: 'white',
    border: '2px solid #164796',
    fontSize: 16,
    padding: '10px 12px',
    marginBottom:0,
    width:'100%',
    fontFamily: [].join(','),
    '&:focus': {
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)'
    }
  }

  }
class ToDoTable extends Component {

  state = {
    todoData:["Example To-Do"],
    inputSearch: false,
    searchKeyWord: ""
  }

  //checks if there is any saved to-do from previous session
  componentDidMount() {
    const storeData = localStorage.getItem('todoData');
    if (storeData === null) return; //return if nothing

    //else retrieve data and set state.
    let items = localStorage.getItem("todoData").split(',');
    this.setState({todoData: items});
  }

  //Generates all of the items in the list, if any.
  getListItems = () => {
    let items = [];
    this.state.todoData.map((item,i) => {
      items.push(
        <ListItem style = {styles.listItemStyle}>
        <ListItemText primary={item} />
        <InputAdornment position="end">
          <IconButton>
            <DeleteIcon
            color="action"
            style= {{color:'#b21825'}}
            onClick = {() => this.removeInput(i)}/>
          </IconButton>
        </InputAdornment>
        </ListItem>
      );
    });
    return items;
  }

  //remove element from list given index e
  removeInput = async (e) => {
    let items = [...this.state.todoData];
    items.splice(e,1);
    await this.setState({todoData:items});
    this.saveData();
  }

  //new function just incase I want to add more filters in the future.
  isEmptyInput = (e) => {
    //ignores empty input
    return e === "";
  }
  //adds the new to-do to the list.
  addToList = async (e) => {
    if (this.isEmptyInput(e)) return;
    let items = [...this.state.todoData];
    items.push(e);
    await this.setState({todoData:items});
    this.saveData();
  }

  //handles input pressed.
  keyPressed = (e) => {
    //when user enters space.
    if (e.keyCode === 13) {
      //ignores empty input
      if (this.isEmptyInput(e.target.value)) return;
      this.addToList(e.target.value);
      e.target.value = "";
    }
  }

  //saves changes to localStorage
  saveData = () => {
    localStorage.setItem('todoData',this.state.todoData);
  }

  //the input to add more to-do
  getInputForm = () => {
    return (
      <FormControl>
       <TextField
       style={styles.inputField}
       helperText="Add new To-Do"
       onKeyDown={this.keyPressed}
       />
      </FormControl>
    )

  }

  //the input to search items in the list
  getSearchForm = () => {
    return (
      <FormControl>o
       <TextField
       style={styles.inputField}
       helperText="Search To-Do"
       onChange={e=> {this.setState({searchKeyWord:e.target.value})}}
       />
      </FormControl>
    )
  }

  //generates the list for search results
  getSearchResult = () => {
    let items = [];
    this.state.todoData.map( (item,i) => {
      //ignore cases when comparing both strings.
       if (item.toLowerCase().startsWith(this.state.searchKeyWord.toLowerCase()))
        items.push(
          <ListItem style = {styles.listItemStyle}>
          <ListItemText primary={item} />
          <InputAdornment position="end">
            <IconButton>
              <DeleteIcon
              color="action"
              style= {{color:'#b21825'}}
              onClick = {() => this.removeInput(i)}/>
            </IconButton>
          </InputAdornment>
          </ListItem>
        );
    });
    return items;
  }

  render () {

    return (
      <div>
        <Grid container justify = "center" style={{marginTop:'3.5%',padding:0, marginBottom:0}}>
          <IconButton style={{marginLeft:'-7%'}} onClick = {() => {this.setState({inputSearch:false})}} >
            <i class="material-icons"> add </i>
          </IconButton>
          <IconButton onClick = {() => {this.setState({inputSearch:true})}}>
            <i class="material-icons"> search </i>
          </IconButton>
          {this.state.inputSearch ? this.getSearchForm():this.getInputForm()}
           <Grid  container justify = "center"  style = {{'overflow-y':'auto', maxHeight:550, paddingTop:0, margin:-8}}>
           <List dense = {false}>
            {this.state.inputSearch ? this.getSearchResult(): this.getListItems()}
          </List>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default ToDoTable;
